﻿/* Напечатать на экране последовательность Фиббоначи из k элементов. 
 * При решении использовать рекурсию. 
 * Нельзя использовать строки, списки, массивы.
 */

using System;
using System.Threading.Tasks;

namespace lab7
{
    class lesson7
    {
        // функция Фибоначчи
        int Fibonachi(int n)
        {
            if (n == 0 || n == 1) return n;

            return Fibonachi(n - 1) + Fibonachi(n - 2);
        }

        public void Execute()
        {
            Console.WriteLine("Введите число");
            Console.Write("X = ");
            int x = int.Parse(Console.ReadLine());

            int j = 0;
            while (j < x)
            {
                Console.WriteLine($"Итерация {j+1}, число Фиббоначи {Fibonachi(j)}");
                j++;
            }
        }
    }
}

