﻿/*Напечатайте на экране монитора числа, 
 * принадлежащие отрезку [1; 99] и кратные числу 3.
 */

using System;
using System.Threading.Tasks;

namespace lab1
{
    class lesson1
    {
        public void Execute()
        {
            for (int i = 0; i < 100; i++)
            {
                if (i % 3 == 0)
                    Console.WriteLine(i);
            }
        }
    }
}
