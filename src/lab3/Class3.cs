﻿/*Датчиком случайных чисел сгенерируйте четыре натуральных числа в интервале от 1 до 900. 
 * Определите, сколько цифр содержит сумма полученных чисел. 
 * Нельзя использовать строки, списки, массивы.
 * Тест. Полученные числа: 567; 41; 138; 862.
 * Результат. Сумма 1608 содержит 4 цифры.
*/

using System;
using System.Threading.Tasks;

namespace lab3
{
    class lesson3
    {
        public void Execute()
        {
            int Sum = 0;
            Random rnd = new Random();
            int value = 0;

            for (int i = 0; i < 4; i++)
            {
                value = rnd.Next(1, 900);
                Console.WriteLine($"{Environment.NewLine}Число {i+1} = {value}");
                Sum = Sum + value;
            }

            if (Sum >= 1000)
                Console.WriteLine($"{Environment.NewLine}Сумма чисел равна {Sum} и содержит 4 цифры");
            else if (Sum <= 999 && Sum >= 100)
                Console.WriteLine($"{Environment.NewLine}Сумма чисел равна {Sum} и содержит 3 цифры");
            else
                Console.WriteLine($"{Environment.NewLine}Сумма чисел равна {Sum} и содержит 2 цифры");
        }
    }
}
