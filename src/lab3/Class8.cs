﻿/* Дано натуральное число N. Вычислите сумму его цифр. 
 * При решении использовать рекурсию. 
 * Нельзя использовать строки, списки, массивы.
 */

using System;
using System.Threading.Tasks;

namespace lab8
{
    class lesson8
    {
        static int recursion(int n)
        {
            if (n < 10)
                return n;
            else
                return n % 10 + recursion(n / 10);
        }

        public void Execute()
        {
            Console.WriteLine("Введите число");
            Console.Write("x = ");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine($"Сумма цифр числа {x} = {recursion(x)}");
        }
    }
}


