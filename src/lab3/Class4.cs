﻿/* Даны четыре числа. Найдите наибольшее и наименьшее среди них, 
 * используя не более четырех сравнений.
*/

using System;
using System.Threading.Tasks;

namespace lab4
{
    class lesson4
    {
        public void Execute()
        {
            Random rnd = new Random();
            int value = 0 , min = 0, max = 0;
            int[] array = new int[4];

            for (int i = 0; i < 4; i++)
            {
                value = rnd.Next(1, 99);
                array[i] = value;
                Console.WriteLine(array[i]);

            }

            max = array[0];
            min = array[0];

            for (int r = 1; r < 4; r++)
            {
                if (max < array[r]) 
                    max = array[r];

                if (min > array[r]) 
                    min = array[r];
            }

            Console.WriteLine(min);
            Console.WriteLine(max);

        }
    }
}
