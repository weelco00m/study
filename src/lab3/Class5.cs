﻿/*Имеются две переменные одинакового типа: целые или вещественные. 
 * Обменяйте их значения: 
 * а) используя дополнительную переменную; 
 * б) не вводя дополнительную переменную.
*/
using System;
using System.Threading.Tasks;

namespace lab5
{
    class lesson5
    {
        public void Execute()
        {
            Console.Write("Введите первое число ");
            var a = Console.ReadLine();
            Console.Write("Введите второе число ");
            var b = Console.ReadLine();

            Console.WriteLine($"{Environment.NewLine}Обменять значения, используя дополнительную переменную, введите 1");
            Console.WriteLine("Обменять значения, не вводя дополнительную переменную, введите 2");
            Console.Write($"{Environment.NewLine}Вы выбрали: ");

            int x = int.Parse(Console.ReadLine());

            int a1, b1, temp1;
            float a2, b2, temp2;


            switch (x)
            {
                case 1:
                    if (int.TryParse(a, out a1) && int.TryParse(b, out b1))
                    {
                        temp1 = a1;
                        a1 = b1;
                        b1 = temp1;

                        Console.WriteLine($"{Environment.NewLine}Перемешиваем целое число используя дополнительную переменную");
                        Console.WriteLine($"{Environment.NewLine}Первое число {a1}");
                        Console.WriteLine($"Второе число {b1}");
                    }

                    else if (float.TryParse(a, out a2) && float.TryParse(b, out b2))
                    {
                        temp2 = a2;
                        a2 = b2;
                        b2 = temp2;

                        Console.WriteLine($"{Environment.NewLine}Перемешиваем вещественное число используя дополнительную переменную");
                        Console.WriteLine($"{Environment.NewLine}Первое число {a2}");
                        Console.WriteLine($"Второе число {b2}");
                    }

                    break;

                case 2:
                    if (int.TryParse(a, out a1) && int.TryParse(b, out b1))
                    {
                        a1 = a1 + b1;
                        b1 = a1 - b1;
                        a1 = a1 - b1;

                        Console.WriteLine($"{Environment.NewLine}Перемешиваем целое число не вводя дополнительную переменную");
                        Console.WriteLine($"{Environment.NewLine}Первое число {a1}");
                        Console.WriteLine($"Второе число {b1}");
                    }

                    else if (float.TryParse(a, out a2) && float.TryParse(b, out b2))
                    {
                        a2 = a2 + b2;
                        b2 = a2 - b2;
                        a2 = a2 - b2;

                        Console.WriteLine($"{Environment.NewLine}Перемешиваем вещественное число не вводя дополнительную переменную");
                        Console.WriteLine($"{Environment.NewLine}Первое число {a2, 1}");
                        Console.WriteLine($"Второе число {b2, 1}");
                    }

                    break;

                default:
                    Console.WriteLine("Введите 1 или 2");
                    break;

            }
            

        }
    }
}
