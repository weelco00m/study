﻿/* Напечатайте на экране все двузначные числа, 
 * кратные данному числу k.
*/

using System;
using System.Threading.Tasks;

namespace lab2
{
    class lesson2
    {
        public void Execute()
        {
            Console.WriteLine("Введите число");
            var t = Console.ReadLine();
            int k;
            Console.WriteLine($"{Environment.NewLine}Числа кратные числу {t}");
            if (int.TryParse(t, out k))
            {
                for (int i = 9; i < 99; i++)
                {
                    if (i % k == 0)
                        Console.WriteLine(i);
                }
            }
        }
    }
}
