﻿using System;
using System.Threading.Tasks;
using lab1;
using lab2;
using lab3;
using lab4;
using lab5;
using lab6;
using lab7;
using lab8;
using lab9;

namespace study
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"{Environment.NewLine}Введите номер лабы от 1 до 9");
            Console.Write($"{Environment.NewLine}Вы выбрали: ");
            int x = int.Parse(Console.ReadLine());

            switch (x)
            {
                    case 1:
                        var lesson1 = new lesson1();
                        lesson1.Execute();
                        break;

                    case 2:
                        var lesson2 = new lesson2();
                        lesson2.Execute();
                        break;

                    case 3:
                        var lesson3 = new lesson3();
                        lesson3.Execute();
                        break;

                    case 4:
                        var lesson4 = new lesson4();
                        lesson4.Execute();
                        break;

                    case 5:
                        var lesson5 = new lesson5();
                        lesson5.Execute();
                        break;

                    case 6:
                        var lesson6 = new lesson6();
                        lesson6.Execute();
                        break;

                    case 7:
                        var lesson7 = new lesson7();
                        lesson7.Execute();
                        break;

                    case 8:
                        var lesson8 = new lesson8();
                        lesson8.Execute();
                        break;

                    case 9:
                        var lesson9 = new lesson9();
                        lesson9.Execute();
                        break;

                    default:
                        Console.WriteLine("Введите от 1 до 9");
                        break;
            }
        }
    }
}


