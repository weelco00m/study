﻿/* Дано натуральное число N. 
 * Вычислить количество единиц. При решении использовать рекурсию. 
 * Нельзя использовать строки, списки, массивы.
 */

using System;
using System.Threading.Tasks;

namespace lab9
{
    class lesson9
    {
        public void Execute()
        {



            for (int i = 0; i < 100; i++)
            {
                if (i % 3 == 0)
                    Console.WriteLine(i);
            }
        }
    }
}
