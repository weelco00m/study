﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2._1
{
    class FindString
    {
        public async Task ExecuteAsync()
        {
            Console.Write("Введите слово:  ");
            var a = Console.ReadLine();

            string path = @"c:\lab2.1.txt";
            string search = a;

            // асинхронное чтение
            using (StreamReader reader = new StreamReader(path))
            {


                string? line;

                while ((line = await reader.ReadLineAsync()) != null)
                {
                    bool b = line.Contains(search);

                    if (b)
                    {
                        Console.WriteLine(line);
                    }

                }
            }
        }
    }
}
