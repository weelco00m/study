﻿/*Реализовать стек строк, в который можно добавлять и удалять элементы. 
 * При добавлении и удалении элементов должно запускаться событие.
*/

using System;
using System.Collections.Generic;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            var MyStack = new Stack<string>();

            Boolean y = true;

            while (y)
            {
                Console.WriteLine("\nЗаписать в стек = 1\n" +
                    "Удалить из стека = 2\n" +
                    "Прочесть стек = 3\n" +
                    "Очистить стек = 4\n" +
                    "Выйти = 0\n");
                var x = Convert.ToInt32(Console.ReadLine());
               // Convert.ToInt32(Console.ReadLine())

                if (x == 1)
                {
                    Console.Write("Записать в стек ");
                    var a = Console.ReadLine();
                    MyStack.Push(a);
                }

                if (x == 2)
                {
                    Console.Write("Удалена запись из стека: ");
                    // var a = Console.ReadLine();
                    string headPerson = MyStack.Peek();
                    Console.WriteLine(headPerson);  // Bob
                    MyStack.Pop();
                }

                if (x == 3)
                {
                    int i = 1;
                    
                    if (MyStack.Count == 0)
                        Console.WriteLine($"Стек пуст");

                    else
                    {
                        foreach (string s in MyStack)
                        {
                            Console.WriteLine($"Строка {i} - {s}");
                            i++;
                        }
                    }
                    Console.WriteLine();
                }


                if (x == 4)
                {
                    Console.Write("Стек очищен");
                    MyStack.Clear();
                }

                if (x == 0)
                {
                    break;
                }
            }




            //do
            //     {

            //        Console.WriteLine("Записать в стек ");
            //        var a = Console.ReadLine();
            //        MyStack.Push(a);
            //        Console.WriteLine("Хотите продолжить? нажмите Y");
            //    }

            //    while (Console.ReadLine().ToLower() == "y");





            // MyStack.Push('N');
            // MyStack.Push('X');

            // Console.WriteLine("Исходный стек: ");


            // foreach (string s in MyStack)
            //     Console.WriteLine(s);
            // Console.WriteLine("\n");

            //  while (MyStack.Count > 0)
            // {
            //     Console.WriteLine("Pop -> {0}", MyStack.Pop());
            // }

            // if (MyStack.Count == 0)
            //    Console.WriteLine("\nСтек пуст!");

            //  Console.ReadLine();
        }
    }
}
