﻿using bgTeam.DataAccess.Impl.PostgreSQL;
using Dapper;
using System;

namespace SpaceObjeckt
{
    class bdRead
    {
        public static void ReadData(string type)
        {
            var cs = new ConnectionSetting()
            {
                ConnectionString = "Server=localhost; User Id=postgres; Database=Testbd;  Password=Qwert12345; Port=5432"
            };

            var factory = new ConnectionFactory(cs);

            using (var connection = factory.Create())
            {

                var reader = connection.Query($"SELECT * FROM {type}");

                Console.WriteLine($"{"ID",3}\t{"Name",22}\t{"Weight",20}\t{"Radius",20}");
                foreach (var obj in reader)
                {
                    Console.WriteLine($"{obj.id,3}\t{obj.name,22}\t{obj.weight,20}\t{obj.radius,20}");
                }

            }
        }
    }
}
