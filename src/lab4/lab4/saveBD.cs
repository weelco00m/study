﻿using bgTeam.DataAccess.Impl.PostgreSQL;
using Dapper;


namespace SpaceObjeckt
{
    public class saveBD
    {

        public static void saveData(string type, string _name, double _weight, double _radius)
        {

            var cs = new ConnectionSetting()
            {
                ConnectionString = "Server=localhost; User Id=postgres; Database=Testbd;  Password=Qwert12345; Port=5432"
            };
         
            var factory = new ConnectionFactory(cs);

            using (var connection = factory.Create())
            {

                switch (type)
                {
                    case "star":
                        Star star = new Star()
                        {
                            name = _name,
                            weight = _weight,
                            radius = _radius
                        };

                        connection.Execute("INSERT INTO star (name, weight, radius)VALUES(@name, @weight, @radius)", star);

                        break;

                    case "asteroid":
                        Asteroid asteroid = new Asteroid()
                        {
                            name = _name,
                            weight = _weight,
                            radius = _radius
                        };

                        connection.Execute("INSERT INTO asteroid (name, weight, radius)VALUES(@name, @weight, @radius)", asteroid);

                        break;

                    case "planet":
                        Planet planet = new Planet()
                        {
                            name = _name,
                            weight = _weight,
                            radius = _radius
                        };

                        connection.Execute("INSERT INTO planet (name, weight, radius)VALUES(@name, @weight, @radius)", planet);

                        break;

                    case "blackhole":
                        BlackHole blackhole = new BlackHole()

                        {
                            name = _name,
                            weight = _weight,
                            radius = _radius
                        };

                        connection.Execute("INSERT INTO blackhole (name, weight, radius)VALUES(@name, @weight, @radius)", blackhole);

                        break;
                }
            }
        }
    }
}
