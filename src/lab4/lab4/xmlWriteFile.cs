﻿using System.IO;
using System.Xml.Linq;

namespace SpaceObjeckt
{
    
    // Класс для записи в файлы XML с методами разбитыми по классам объектов
    public class xmlWriteFile
    {

        public static void WriterData(string type, string name, double weight, double radius)
        {
            //каталог сохранения
            string dataDirPath = @$"C:\lab4\data";
            //файл сохранения
            string fileName = @$"C:\lab4\data\{type}.xml";

            //Создаем папку ("dataDirPath") для сохранения файла, если ее нет
            if (!Directory.Exists(dataDirPath))
                Directory.CreateDirectory(dataDirPath);  
           
            //если XML файл существует ("fileName"), то добавляем инфу
            FileInfo fileInfo = new FileInfo(fileName);
            if (fileInfo.Exists)
            {
                //используем функциональность LINQ to XML 
                //создадим новый XML - документ
                XDocument xdoc = XDocument.Load(fileName);
                XElement? root = xdoc.Element("Space_object");

                if (root != null)
                {
                    // добавляем данные в XML - документ
                    root.Add(new XElement("Object",
                        new XAttribute("name", name),
                        new XElement("weight", weight),
                        new XElement("radius", radius)));
                        xdoc.Save(fileName);
                }
            }

            //иначе создаем новый XML - документ и записываем данные
            else
            {
                XDocument xdoc = new XDocument(
                    new XElement("Space_object",
                    new XElement("Object",
                    new XAttribute("name", name),
                    new XElement("weight", weight),
                    new XElement("wadius", radius))));
                xdoc.Save(fileName);
             }

            saveBD.saveData(type, name, weight, radius);
        }
    }      
}


