﻿using System;

namespace SpaceObjeckt
{
    class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {
                Console.WriteLine("\nВыберите действие:" +
                                  "\n1 - Добавить запись о космическом объекте" +
                                  "\n2 - Просмотреть список космических объектов" +
                                  "\n3 - Удалить информацию об объекте" +
                                  "\n0 - Выйти");

                //Посмотреть объекты о которых имеется информация

                var n = Console.ReadLine();
                if (n == "0") break;

                switch (n)
                {
                    case "1":
                        Console.WriteLine();
                        Console.WriteLine("Добавить объект:" +
                            "\n1 - Звезда" +
                            "\n2 - Планета" +
                            "\n3 - Астероид" +
                            "\n4 - Черная дыра" +
                            "\n0 - Вернуться в предыдущее меню.");

                        var m = Console.ReadLine();
                        Console.WriteLine();
                        if (m == "0") break;

                        switch (m)
                        {
                            case "1":
                                Star star = new Star();
                                console.enterDate(star.type, star.objekt);
                                break;

                            case "2":
                                Planet planet = new Planet();
                                console.enterDate(planet.type, planet.objekt);
                                break;

                            case "3":
                                Asteroid asteroid = new();
                                console.enterDate(asteroid.type, asteroid.objekt);
                                break;

                            case "4":
                                BlackHole blackhole = new BlackHole();
                                console.enterDate(blackhole.type, blackhole.objekt);
                                break;
                        }
                        break;

                    case "2":
                        Console.WriteLine();
                        Console.WriteLine("Просмотр объектов:" +
                            "\n1 - Звезда" +
                            "\n2 - Планета" +
                            "\n3 - Астероид" +
                            "\n4 - Черная дыра" +
                            //"\n5 - BD Звезда" +
                            "\n0 - Вернуться в предыдущее меню.");

                        var j = Console.ReadLine();
                        Console.WriteLine();
                        if (j == "0") break;

                        switch (j)
                        {
                            case "1":
                                //"подключаем" класс Star
                                Star star = new Star();
                                // и передаем параметры для "конструктора (ReadFile.cs)" XML файла 
                                xmlReadFile.ViewerData(star.type, star.objekt);
                                bdRead.ReadData(star.type);
                                break;
                            case "2":
                                Planet planet = new Planet();
                                xmlReadFile.ViewerData(planet.type, planet.objekt);
                                bdRead.ReadData(planet.type);
                                break;
                            case "3":
                                Asteroid asteroid = new Asteroid();
                                xmlReadFile.ViewerData(asteroid.type, asteroid.objekt);
                                bdRead.ReadData(asteroid.type);
                                break;
                            case "4":
                                BlackHole blackhole = new BlackHole();
                                xmlReadFile.ViewerData(blackhole.type, blackhole.objekt);
                                bdRead.ReadData(blackhole.type);
                                break;

                        }
                        break;

                    case "3":
                        Console.WriteLine();
                        Console.WriteLine("Для удаления выберите тип объекта и введите его имя:" +
                            "\n1 - Звезда" +
                            "\n2 - Планета" +
                            "\n3 - Астероид" +
                            "\n4 - Черная дыра" +
                            "\n0 - Вернуться в предыдущее меню.");

                        var l = Console.ReadLine();
                        Console.WriteLine();
                        if (l == "0") break;

                        switch (l)
                        {
                            case "1":
                                //"подключаем" класс Star
                                Star stars = new Star();
                                console.removestring(stars.type, stars.objekt);
                                break;

                            case "2":
                                Planet planet = new Planet();
                                console.removestring(planet.type, planet.objekt);
                                break;

                            case "3":
                                Asteroid asteroid = new Asteroid();
                                console.removestring(asteroid.type, asteroid.objekt);
                                break;

                            case "4":
                                BlackHole blackhole = new BlackHole();
                                console.removestring(blackhole.type, blackhole.objekt);
                                break;

                        }
                        break;

                    default:
                        Console.WriteLine("\nТакого пункта не существует.");
                        break;
                }
            }
        }
    }
}
