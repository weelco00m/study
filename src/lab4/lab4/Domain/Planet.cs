﻿
namespace SpaceObjeckt
{
    public class Planet
    {
        //тип объекта 
        public string type { get; set; } = "planet";
        //название объекта 
        public string objekt { get; set; } = "Планета";

        public string name { get; set; }

        public double weight { get; set; }

        public double radius { get; set; }

    }
}
