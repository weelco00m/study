﻿
namespace SpaceObjeckt
{
    public class Star
    {
        //тип объекта 
        public string type { get; set; } = "star";
        //название объекта 
        public string objekt { get; set; } = "Звезда";

        public int id { get; set; }

        public string name { get; set; } 

        public double weight { get; set; }

        public double radius { get; set; }
    }
}
