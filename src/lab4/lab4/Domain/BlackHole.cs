﻿
namespace SpaceObjeckt
{
    public class BlackHole
    {
        //тип объекта
        public string type { get; set; } = "blackhole";
        //название объекта
        public string objekt { get; set; } = "Черная дыра";

        public string name { get; set; }

        public double weight { get; set; }

        public double radius { get; set; }
    }
}
