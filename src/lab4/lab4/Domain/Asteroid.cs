﻿
namespace SpaceObjeckt
{
    public class Asteroid
    {
        //тип объекта
        public string type { get; set; } = "asteroid";
        //название объекта
        public string objekt { get; set; } = "Астероид";

        public string name { get; set; }

        public double weight { get; set; }

        public double radius { get; set; }
    }
}
