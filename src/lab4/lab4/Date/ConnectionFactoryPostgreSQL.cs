﻿namespace bgTeam.DataAccess.Impl.PostgreSQL
{
    using System;
    using System.Data;
    using System.Threading.Tasks;
    using SpaceObjeckt;
    using Npgsql;

    public interface IConnectionFactory
    {
        /// <summary>
        /// Создаёт подключение к базе данных
        /// </summary>
        /// <returns>Объект подключения</returns>
        IDbConnection Create();

        /// <summary>
        /// Создаёт подключение к базе данных по переданной строке подключения
        /// </summary>
        /// <returns>Объект подключения</returns>
        IDbConnection Create(string connectionString);

        /// <summary>
        /// Асинхронно создаёт подключение к базе данных
        /// </summary>
        /// <returns>Объект подключения</returns>
        Task<IDbConnection> CreateAsync();

        /// <summary>
        /// Асинхронно создаёт подключение к базе данных по переданной строке подключения
        /// </summary>
        /// <returns>Объект подключения</returns>
        Task<IDbConnection> CreateAsync(string connectionString);
    }

    public class ConnectionFactory : IConnectionFactory
    {
        private readonly IConnectionSetting _setting;
        private readonly string connectionString;

        public ConnectionFactory(IConnectionSetting setting)
        {
            _setting = setting ?? throw new ArgumentNullException(nameof(setting));
            if (string.IsNullOrEmpty(_setting.ConnectionString))
            {
                throw new ArgumentOutOfRangeException(nameof(setting), "Connection string should not be null or empty");
            }


            //public ConnectionFactory(string settings)
            //{
            //    this._setting = settings;
            //}





            //if (dialect != null)
            //{
            //    dialect.Init(SqlDialectEnum.PostgreSql);
            //}
        }

        public ConnectionFactory(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IDbConnection Create()
        {
            return Create(_setting.ConnectionString);
        }

        public IDbConnection Create(string connectionString)
        {
            NpgsqlConnection dbConnection = new NpgsqlConnection(connectionString);
            dbConnection.Open();
            return dbConnection;
        }

        public async Task<IDbConnection> CreateAsync()
        {
            return await CreateAsync(_setting.ConnectionString);
        }

        public async Task<IDbConnection> CreateAsync(string connectionString)
        {
            NpgsqlConnection dbConnection = new NpgsqlConnection(connectionString);
            await dbConnection.OpenAsync().ConfigureAwait(false);
            return dbConnection;
        }
    }
}


//namespace lab4
//{
//    using Npgsql;
//    using System;
//    using System.Data;
//    using System.Threading.Tasks;

//    public interface IConnectionFactory
//    {
//        /// <summary>
//        /// Создаёт подключение к базе данных
//        /// </summary>
//        /// <returns>Объект подключения</returns>
//        IDbConnection Create();

//        /// <summary>
//        /// Создаёт подключение к базе данных по переданной строке подключения
//        /// </summary>
//        /// <returns>Объект подключения</returns>
//        IDbConnection Create(string connectionString);

//        /// <summary>
//        /// Асинхронно создаёт подключение к базе данных
//        /// </summary>
//        /// <returns>Объект подключения</returns>
//        Task<IDbConnection> CreateAsync();

//        /// <summary>
//        /// Асинхронно создаёт подключение к базе данных по переданной строке подключения
//        /// </summary>
//        /// <returns>Объект подключения</returns>
//        Task<IDbConnection> CreateAsync(string connectionString);
//    }

//    public class ConnectionFactory : IConnectionFactory
//    {
//        //private readonly IConnectionSetting _setting;
//        private readonly string _settings;
//        public ConnectionFactory(string settings)
//        {
//            this._settings = settings;
//        }
//        //public ConnectionFactory(IConnectionSetting setting)
//        //{
//        //    _setting = setting ?? throw new ArgumentNullException(nameof(setting));
//        //    if (string.IsNullOrEmpty(_setting.ConnectionString))
//        //    {
//        //        throw new ArgumentOutOfRangeException(nameof(setting), "Connection string should not be null or empty");
//        //    }


//        //}

//        public IDbConnection Create()
//        {
//            return Create(_settings);
//        }

//        public IDbConnection Create(string connectionString)
//        {
//            NpgsqlConnection dbConnection = new NpgsqlConnection(connectionString);
//            dbConnection.Open();
//            return dbConnection;
//        }

//        public async Task<IDbConnection> CreateAsync()
//        {
//            return await CreateAsync(_settings);
//        }

//        public async Task<IDbConnection> CreateAsync(string connectionString)
//        {
//            NpgsqlConnection dbConnection = new NpgsqlConnection(connectionString);
//            await dbConnection.OpenAsync().ConfigureAwait(false);
//            return dbConnection;
//        }
//    }
//}
