﻿
namespace SpaceObjeckt
{
    public interface IConnectionSetting
    {
        string ConnectionString { get; set; }
    }

    public class ConnectionSetting : IConnectionSetting
    {
        public string ConnectionString { get; set; }
    }
}
