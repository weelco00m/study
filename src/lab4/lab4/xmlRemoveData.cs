﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace SpaceObjeckt
{
    class xmlRemoveData
    {
        // удалениe записи об "объекте" из файла XML
        public static void RemoveData(string type, string objekt, string removeName)
        {
            //файл сохранения
            string fileName = @$"C:\lab4\data\{type}.xml";

            //если XML файл существует ("fileName"), то читаем инфу
            FileInfo fileInfo = new FileInfo(fileName);
            if (fileInfo.Exists)
            {
                XDocument xdoc = XDocument.Load(fileName);
                XElement? root = xdoc.Element("Space_object");

                if (root != null)
                {
                    // получим элемент root с name = 
                    var del1 = root.Elements("Object").FirstOrDefault(p => p.Attribute("name")?.Value == removeName);
                    // и удалим его
                    if (del1 != null)
                    {
                        del1.Remove();
                        xdoc.Save(fileName);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"\nОбъект {removeName} удален");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"\nОбъекта {removeName} нет");
                        Console.ResetColor();
                    }
                }
            }

            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nСписок объектов \"{objekt}\" пуст");
                Console.ResetColor();
            }
        }
    }
}
