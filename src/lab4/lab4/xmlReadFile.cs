﻿using System;
using System.IO;
using System.Xml.Linq;

namespace SpaceObjeckt
{
    public class xmlReadFile
    {
        //чтениe иформации об "объектах" из файла XML
        public static void ViewerData(string type, string objekt)
        {
            //файл сохранения
            string fileName = @$"C:\lab4\data\{type}.xml";

            //если XML файл существует ("fileName"), то читаем инфу
            FileInfo fileInfo = new FileInfo(fileName);
            if (fileInfo.Exists)
            {

                XDocument xdoc = XDocument.Load(fileName);
                // получаем корневой узел
                XElement? findInfo = xdoc.Element("Space_object");
                if (findInfo is not null)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{"Type",4}\t{"Name",18}\t{"Weight",15}\t{"Radius",15}");
                    Console.ResetColor();

                    // проходим по всем элементам person
                    foreach (XElement objects in findInfo.Elements("Object"))
                    {
                            XAttribute? name = objects.Attribute("name");
                            XElement? weight = objects.Element("weight");
                            XElement? radius = objects.Element("radius");

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"{objekt,4}\t{name?.Value,10}\t{weight?.Value,15}\t{radius?.Value,15}");
                        Console.ResetColor();
                    }
                }
            }

            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"\nСписок объектов \"{objekt}\" пуст");
                Console.ResetColor();
            }
        }
    }
}
