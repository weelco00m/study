﻿using FluentMigrator;

namespace Duktus.Migrations._2021
{
    [Migration(2021042518312, "DKS-581")]
    public class Migration_202104251831 : Migration
    {
        public override void Up()
        {
            Create.Table("star")
                .WithColumn("id").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(256)
                .WithColumn("weight").AsInt32()
                .WithColumn("radius").AsInt32();

            Create.Table("planet")
                .WithColumn("id").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(256)
                .WithColumn("weight").AsInt32()
                .WithColumn("radius").AsInt32();

            Create.Table("asteroid")
                .WithColumn("id").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(256)
                .WithColumn("weight").AsInt32()
                .WithColumn("radius").AsInt32();

            Create.Table("blackhole")
                .WithColumn("id").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(256)
                .WithColumn("weight").AsInt32()
                .WithColumn("radius").AsInt32();
        }

        public override void Down()
        {
            //Delete.Table("star");
            //Delete.Table("planet");
            //Delete.Table("asteroid");
            //Delete.Table("blackhole");

        }
    }
}