using System;
using FluentMigrator.Runner;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;



namespace Duktus.Migrations
{
    public static class Program
    {
        private const string DATABASE_NAME = "DBTEST";

        public static void Main()
        {
            var serviceProvider = CreateServices();

            using (var scope = serviceProvider.CreateScope())
            {
                UpdateDatabase(scope.ServiceProvider);
            }
        }

        private static IServiceProvider CreateServices()
        {
            //var env = Environment.GetEnvironmentVariable(ENVIRONMENT_VARIABLE_NAME);
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                // .AddJsonFile($"Configs/connectionStrings.{env}.json")
                // .AddEnvironmentVariables()
                .Build();

            return new ServiceCollection()
                .AddSingleton<IConfiguration>(config)
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddPostgres10_0()
                    .WithGlobalConnectionString(x => x.GetRequiredService<IConfiguration>().GetConnectionString(DATABASE_NAME))
                    .WithGlobalCommandTimeout(TimeSpan.FromMinutes(5))
                    .ScanIn(typeof(Program).Assembly).For.Migrations())
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .BuildServiceProvider(false);
        }

        private static void UpdateDatabase(IServiceProvider serviceProvider)
        {
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();
            runner.MigrateUp();
        }
    }


}